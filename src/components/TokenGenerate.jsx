import {useState} from "react";
import $ from 'jquery'
import {Stack, Typography} from "@mui/material";
import PaginationPage from "./PaginationPage";
import ResultsPageElements from "./ResultsPageElements";
import * as React from "react";
import axios from "axios";

export default function TokenGenerate() {
    const [token, setToken] = useState();
    const [tokenState, setTokenState] = useState('');

    const getStaticProps = async () => {
        // const res = await fetch("http://127.0.0.1:8000/api/auth/token/login", {
        //     body: '{"email":"qwe@qwe.com","password":"1"}',
        //     headers: {
        //         "Content-Type": "application/json"
        //     },
        //     method: "POST"
        // })
        // let data = await res.json()
        //
        // const loggedData = await axios.get("/api/auth/logout");
        // let email = 'qwe@qwe.com;'
        // let password = '1';
        // const credentials = { email, password };
        // const data = await axios.post("/api/auth/login", credentials);
        // console.log(data)
        // console.log(loggedData);
        // setTokenState(true)
        // setToken(data.token)
    }

    const handleClick = (e) => {
        const tokenArea = document.getElementById('tokenArea');
        switch(e) {
            case 'get':
                if ( tokenArea ){
                    tokenArea.value = ''
                }
                getStaticProps()
                break;
            case 'copy':
                if ( tokenArea && tokenArea.value.length ){
                    let text = tokenArea.value
                    navigator.clipboard.writeText(text)
                        .then(() => {
                            $('.alert-success').show()
                            setTimeout( function (){
                                $('.alert-success').fadeOut("slow");
                            }, 1000)
                            console.log('Text copied to clipboard');
                        })
                        .catch(err => {
                            console.error('Error in copying text: ', err);
                        });
                }
                break;
            default:
                console.log('Error')
        }
    }

    return (
        <div className="row gx-5">
            <div className="col-9">
                <Stack spacing={3}>
                    {tokenState ?
                        <PaginationPage>
                            {(i) => (
                                <ResultsPageElements
                                    index={i}
                                    token={token}
                                />
                            )}
                        </PaginationPage>
                    :
                        <Typography variant='h5'>
                            No token!
                        </Typography>
                    }
                </Stack>
            </div>
            <div className="col-3">
                <div className="p-3 tableCard">
                    <button className="btn btn-sm btn-primary"
                            onClick={() => handleClick('get')}
                    >
                        New token
                    </button>
                    <button className="btn btn-sm btn-primary" style={{marginLeft: '15px'}}
                            onClick={() => handleClick('copy')}
                    >
                        Copy Token
                    </button>
                    {token ?
                        <textarea style={{marginTop: "15px"}} className="form-control" aria-label="Default"
                                  aria-describedby="inputGroup-sizing-default" disabled="disabled" rows="20"
                                  value={token} id="tokenArea"
                        />
                        :
                        <input style={{marginTop: "15px"}} className="form-control" type="text" disabled="disabled" placeholder="Token"/>
                    }
                </div>
                <div className="alert alert-success" role="alert" style={{marginTop: '15px', display: 'none'}}>
                    Text copied to clipboard
                </div>
            </div>
        </div>
    )
}