import {Card, CardContent, Grid, Typography} from '@mui/material'
import React from 'react'
import CustomButton from "./button/CustomButton";
import useSWR from "swr";

export default function ClientPageById({
                                           token,
                                           id,
                                           name,
                                           email,
                                           phone,
                                           extraInfo,
                                           utm_source,
                                           utm_medium,
                                           utm_campaign,
                                           utm_content,
                                           utm_term
                                       }) {


    const handleDeleteClick = async (token, id) => {
        let data = await fetch(`http://127.0.0.1:8000/api/clients/${id}`, {
            headers: {
                Authorization: `Bearer ${token}`,
                'Content-Type': 'application/json',
                'accept': '*/*'
            },
            method: "DELETE"
        })
            .then((response) => response.json())
            .then((responseData) => {
                console.log(responseData);
                return responseData;
            })
            .catch(error => console.warn(error));

        console.log(data)
        // if (error) {
        //     return (
        //         <Typography variant='h5'>
        //             Произошла ошибка при удалении данных... Попробуйте повторить позднее
        //         </Typography>
        //     )
        // }
        if (data && data.code === 401) {
            return (
                <Typography variant='h5'>
                    JWT Token not found
                </Typography>
            )
        }
        return (
            <div className="alert alert-success" role="alert" style={{marginTop: '15px', display: 'none'}}>
                Клиент успешно удален!
            </div>
        )
    }

    const handleUpdateClick = async (token, id) => {
        let data = await fetch(`http://127.0.0.1:8000/api/clients/${id}`, {
            headers: {
                Authorization: `Bearer ${token}`,
                'Accept': 'application/json',
                'Content-Type': 'application/merge-patch+json'
            },
            body: JSON.stringify(
                { 'name': '1', 'phone': '2', 'email': '3', 'extraInfo': '4' }
                ),
            method: 'PATCH'
        })
            .then((response) => response.json())
            .then((responseData) => {
                return responseData;
            })
            .catch(error => console.warn(error));

        console.log(data)
        // if (error) {
        //     return (
        //         <Typography variant='h5'>
        //             Произошла ошибка при удалении данных... Попробуйте повторить позднее
        //         </Typography>
        //     )
        // }
        if (data && data.code === 401) {
            return (
                <Typography variant='h5'>
                    JWT Token not found
                </Typography>
            )
        }
        return (
            <div className="alert alert-success" role="alert" style={{marginTop: '15px', display: 'none'}}>
                Клиент успешно удален!
            </div>
        )
    }

    return (
        <Card
            sx={{
                bwidth: '100%',
                borderRadius: '12px',
                border: '1px solid #eee',
                height: '100%',
            }}
        >
            <CardContent sx={{
                display: 'flex',
                gap: '15px',
                height: '100%',
                width: '100%',
                alignItems: 'center'
            }}>
                <Grid container spacing={2}>
                    <Grid item xs={12}>
                        <Typography variant='h4'>General info: </Typography>
                    </Grid>
                    <Grid item xs={6}>
                        <Typography>
                            Name: {name}
                        </Typography>
                    </Grid>
                    <Grid item xs={6}>
                        <Typography>
                            Email: {email}
                        </Typography>
                    </Grid>
                    <Grid item xs={6}>
                        <Typography>
                            Phone: {phone}
                        </Typography>
                    </Grid>
                    <Grid item xs={6}>
                        <Typography>
                            Extra info: {extraInfo}
                        </Typography>
                    </Grid>
                    <hr style={{borderTop: "1px solid #333", width: '95%', marginLeft: '24px', marginTop: '30px'}}/>
                    <Grid item xs={12}>
                        <Typography variant='h4'>Analytics: </Typography>
                    </Grid>
                    <Grid item xs={6}>
                        <Typography>
                            utm_source: {utm_source ? utm_source : '-'}
                        </Typography>
                    </Grid>
                    <Grid item xs={6}>
                        <Typography>
                            utm_medium: {utm_medium ? utm_medium : '-'}
                        </Typography>
                    </Grid>
                    <Grid item xs={6}>
                        <Typography>
                            utm_campaign: {utm_campaign ?utm_campaign : '-' }
                        </Typography>
                    </Grid>
                    <Grid item xs={6}>
                        <Typography>
                            utm_content: {utm_content ? utm_content : '-'}
                        </Typography>
                    </Grid>
                    <Grid item xs={6}>
                        <Typography>
                            utm_term: {utm_term ? utm_term : '-'}
                        </Typography>
                    </Grid>

                </Grid>
            </CardContent>
            <Grid style={{display: 'flex', flexDirection: 'row-reverse'}}>
                <Grid item xs={2} style={{marginRight: '2px', marginBottom: '2px'}}>
                    <CustomButton variant='contained btn btn-danger'
                                  style={{borderRadius: '12px'}}
                                  onClick={() => handleDeleteClick(token, id)}
                                  sx={{marginLeft: 'auto', flexShrink: 0, fontWeight: 600}}
                    >Delete </CustomButton>
                </Grid>
            </Grid>
        </Card>
    )
}

export function getServerSideProps({req, res}) {
    return {
        props: {
            token: req.cookies.TokenJWT || ""
        }
    }
}