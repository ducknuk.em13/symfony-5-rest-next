import * as React from "react";
import {Container} from "@mui/material";
import {useState} from "react";
import axios from "axios";
import $ from "jquery";
import { useRouter } from 'next/router'

export default function LoginForm() {
    const router = useRouter()
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const handleSubmit = async (e) => {
        e.preventDefault();
        await axios.get("/api/auth/logout");

        const userLoginData = { email: email, password: password };
        const result = await fetch("http://127.0.0.1:8000/api/auth/token/login", {
            body: JSON.stringify(userLoginData),
            headers: {
                "Content-Type": "application/json"
            },
            method: "POST"
        })
        let data = await result.json()
        if ( data.code === 401 ){
            $('.alert-danger').hide().show()
            setTimeout( function (){
                $('.alert-danger').fadeOut("slow");
            }, 2000)
        } else {
            fetch('api/auth/login', {
                method: 'POST',
                headers:{
                    "Content-Type": "application/json",
                },
                body: JSON.stringify({token: data.token })
            }).then(router.push(`/home`))
        }
    };

    return (
        <Container>
            <div className="container py-5 h-100">
                <div className="alert alert-danger" role="alert" style={{marginTop: '15px', display: 'none'}}>
                    Invalid credentials!
                </div>
                <div className="row d-flex align-items-center justify-content-center h-100">
                    <div className="col-md-8 col-lg-7 col-xl-6">
                        <img src="https://mdbcdn.b-cdn.net/img/Photos/new-templates/bootstrap-login-form/draw2.svg"
                             className="img-fluid" alt="Phone image"/>
                    </div>
                    <div className="col-md-7 col-lg-5 col-xl-5 offset-xl-1">
                        <form onSubmit={(e) => handleSubmit(e)}>
                            <div className="form-outline mb-4">
                                <input
                                    className="form-control form-control-lg"
                                    type="email"
                                    id="form1_1"
                                    name="email"
                                    onChange={(e) => setEmail(e.target.value)}
                                />
                                <label className="form-label" htmlFor="form1_1">Email address</label>
                            </div>

                            <div className="form-outline mb-4">
                                <input
                                    className="form-control form-control-lg"
                                    type="password"
                                    id="form1_2"
                                    name="password"
                                    onChange={(e) => setPassword(e.target.value)}
                                />
                                <label className="form-label" htmlFor="form1_2">Password</label>
                            </div>
                            <button className="btn btn-primary btn-lg btn-block">Sign in</button>
                        </form>
                    </div>
                </div>
            </div>
        </Container>
    )
}