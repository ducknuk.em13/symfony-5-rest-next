import {Button, Stack, Typography} from '@mui/material'
import React, { Fragment, useState } from 'react'
import ChevronRightIcon from '@mui/icons-material/ChevronRight'

export default function PaginationPage({ children, isActive = true }) {
    const [cnt, setCnt] = useState(1)
    const pages = []
    for (let i = 1; i <= cnt; i++) {
        pages.push(<Fragment key={i}>{children(i)}</Fragment>)
    }
    return (
        <>
            {pages}
            {!!pages && !!pages.length && isActive && (
                <Stack width={'100%'} justifyContent='center' alignItems='center'>
                    <Button variant='contained' onClick={() => setCnt(cnt + 1)}>
                        <Typography variant='subtitle2'>Показать еще</Typography>
                        <ChevronRightIcon/>
                    </Button>
                </Stack>
            )}
        </>
    )
}