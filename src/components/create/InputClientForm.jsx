import React, {useState} from 'react'
import {Card, CardContent, Container, Grid, Typography} from "@mui/material";
import $ from 'jquery'


export default function InputClientForm({token}) {
    const [name, setName] = useState("");
    const [phone, setPhone] = useState("");
    const [email, setEmail] = useState("");
    const [extraInfo, setExtraInfo] = useState("");
    const [source, setSource] = useState("");
    const [medium, setMedium] = useState("");
    const [campaign, setCampaign] = useState("");
    const [content, setContent] = useState("");
    const [term, setTerm] = useState("");

    const handleSubmit = async (e) => {
        e.preventDefault();
        let params = new FormData();
        params.append('name', name);
        params.append('phone', phone);
        params.append('email', email);
        params.append('extraInfo', extraInfo);
        params.append('utmSource', source);
        params.append('utmMedium', medium);
        params.append('utmCampaign', campaign);
        params.append('utmContent', content);
        params.append('utmTerm', term);

        let data = await fetch(`http://127.0.0.1:8000/api/create/client`, {
            headers: {
                'Accept': 'application/json',
                Authorization: `Bearer ${token}`,
            },
            body: params,
            method: 'post'
        });
        let responseData = await data.json();

        console.log(responseData)

        if ( responseData === 'Success'){
            $('.alert-success').show()
            setTimeout(function (){
                $('.alert-success').fadeOut('slow')
            }, 2000)
        }
    };


    return (
    <>
        <div className="alert alert-success" role="alert" style={{marginTop: '15px', display: 'none'}}>
            Client was deleted successfully!
        </div>
        <Card
            sx={{
                width: '100%',
                borderRadius: '12px',
                border: '1px solid #eee',
                height: '100%',
            }}
        >
            <form id="client_form" onSubmit={(e) => handleSubmit(e)}>
                <CardContent sx={{
                    display: 'flex',
                    flexDirection: 'column',
                    gap: '15px',
                    height: '100%',
                    width: '100%',
                }}>
                    <div className="row">
                        <div className="col-md-3">
                            <div className="form-outline">
                                <input
                                    className="form-control form-control"
                                    type="text"
                                    id="form1_1"
                                    name="name"
                                    placeholder="Name"
                                    required="required"
                                    onChange={(e) => setName(e.target.value)}
                                />
                            </div>
                        </div>
                        <div className="col-md-3">
                            <div className="form-outline">
                                <input
                                    className="form-control form-control"
                                    type="text"
                                    id="form1_2"
                                    name="phone"
                                    placeholder="Phone"
                                    required="required"
                                    onChange={(e) => setPhone(e.target.value)}
                                />
                            </div>
                        </div>
                        <div className="col-md-3">
                            <div className="form-outline">
                                <input
                                    className="form-control form-control"
                                    type="email"
                                    id="form1_2"
                                    name="email"
                                    placeholder="Email"
                                    required="required"
                                    onChange={(e) => setEmail(e.target.value)}
                                />
                            </div>
                        </div>
                        <div className="col-md-3">
                            <div className="form-outline">
                                <input
                                    className="form-control form-control"
                                    type="text"
                                    id="form1_2"
                                    name="extraInfo"
                                    placeholder="Extra information"
                                    required="required"
                                    onChange={(e) => setExtraInfo(e.target.value)}
                                />
                            </div>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-md-3">
                            <div className="form-outline form-outline-margin">
                                <input
                                    className="form-control form-control"
                                    type="text"
                                    id="form1_1"
                                    name="utmSource"
                                    placeholder="utm_source"
                                    required="required"
                                    onChange={(e) => setSource(e.target.value)}
                                />
                            </div>
                        </div>
                        <div className="col-md-3">
                            <div className="form-outline form-outline-margin">
                                <input
                                    className="form-control form-control"
                                    type="text"
                                    id="form1_1"
                                    name="utmMedium"
                                    placeholder="utm_medium"
                                    required="required"
                                    onChange={(e) => setMedium(e.target.value)}
                                />
                            </div>
                        </div>
                        <div className="col-md-3">
                            <div className="form-outline form-outline-margin">
                                <input
                                    className="form-control form-control"
                                    type="text"
                                    id="form1_1"
                                    name="utmCampaign"
                                    placeholder="utm_campaign"
                                    required="required"
                                    onChange={(e) => setCampaign(e.target.value)}
                                />
                            </div>
                        </div>
                        <div className="col-md-3">
                            <div className="form-outline form-outline-margin">
                                <input
                                    className="form-control form-control"
                                    type="text"
                                    id="form1_1"
                                    name="utmContent"
                                    placeholder="utm_content"
                                    required="required"
                                    onChange={(e) => setContent(e.target.value)}
                                />
                            </div>
                        </div>
                        <div className="col-md-3">
                            <div className="form-outline form-outline-margin">
                                <input
                                    className="form-control form-control"
                                    type="text"
                                    id="form1_1"
                                    name="utmTerm"
                                    placeholder="utm_term"
                                    required="required"
                                    onChange={(e) => setTerm(e.target.value)}
                                />
                            </div>
                        </div>
                        <div className="col-md-3">
                            <button className="btn btn-primary btn-block">Post</button>
                        </div>
                    </div>
                </CardContent>
            </form>

        </Card>
    </>
    )
}