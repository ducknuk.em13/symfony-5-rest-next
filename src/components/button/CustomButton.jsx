import { Button, styled, useMediaQuery, useTheme } from '@mui/material'

const CustomButton = styled(Button)`
	letter-spacing: normal;
	text-transform: none;
`

const ResponsiveCustomButton = (props) => {
    const theme = useTheme()
    const desktop = useMediaQuery(theme.breakpoints.up('lg'))
    const tablet = useMediaQuery(theme.breakpoints.up('sm'))
    const mobile = useMediaQuery(theme.breakpoints.up('xs'))

    const sizes = () => {
        if (desktop) return 'large'
        if (tablet) return 'medium'
        if (mobile) return 'small'
    }

    return <CustomButton size={sizes()} {...props} />
}
export default ResponsiveCustomButton
