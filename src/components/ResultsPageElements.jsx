import {useRouter} from 'next/router'
import React, {useState} from 'react'
import {Grid, Typography} from '@mui/material'
import ResultItem from "./ResultItem";
import useSWR from "swr";

export default function ResultsPageElements({index, token, order}) {
    const router = useRouter()
    let url = `http://127.0.0.1:8000/api/clients?page=${index}&${order}`;
    const fetcher = (url) =>
        fetch(url, {
            method: 'GET',
            headers: {
                Authorization: `Bearer ${token}`,
                'Content-Type': 'application/json',
            }})
            .then((res) => res.json());
    const { data , error } = useSWR(url, fetcher);
    if (error){
        return (
            <Typography variant='h5'>
                Произошла ошибка при загрузке данных... Попробуйте повторить позднее
            </Typography>
        )
    }
    if (data && data.code === 401 ){
        return (
            <Typography variant='h5'>
                JWT Token not found
            </Typography>
        )
    }
    const handleClick = (id) => {
        router.push(`/client/${id}`)
    }
    return (
        <>
            <Grid container sx={{
                display: 'flex',
                flexDirection: 'column',
                gap: '15px'
            }}>
                {data ? data['hydra:member'].map((item, i) => (
                    <ResultItem
                        key={i}
                        id = {item.id}
                        name = {item.name}
                        email = {item.email}
                        phone = {item.phone}
                        onClick={() => handleClick(item.id)}
                    />
                )) :
                    <Typography variant='h5'>
                    Loading...
                </Typography>
                }
            </Grid>
        </>
    )
}
