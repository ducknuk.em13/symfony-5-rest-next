import {Button, Card, CardActionArea, CardContent, Grid, Typography} from '@mui/material'
import React from 'react'
import CustomButton from "./button/CustomButton";

export default function ResultItem({ name, email, phone, onClick,  }) {

    return (
        <Card
            sx={{
                width: '100%',
                borderRadius: '12px',
                border: '1px solid #eee',
                height: '100%',
            }}
        >
            <CardContent sx={{
                display: 'flex',
                gap: '15px',
                height: '100%',
                width: '100%',
                alignItems: 'center'
            }}>
                <Grid item xs={12} md={3}>
                    <Typography>{name}</Typography>
                </Grid>
                <Grid item xs={12} md={4}>
                    <Typography>
                        {email}
                    </Typography>
                </Grid>
                <Grid item xs={12} md={3}>
                    <Typography>{phone}</Typography>
                </Grid>
                <CustomButton variant='contained'
                              onClick={() => onClick()}
                              sx={{marginLeft: 'auto', flexShrink: 0, fontWeight: 600}}
                >Подробнее </CustomButton>
            </CardContent>
        </Card>
    )
}