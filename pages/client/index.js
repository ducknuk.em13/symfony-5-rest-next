import Head from "next/head";
import {Container} from "@mui/material";
import * as React from "react";
import ResultsPageElements from "../../src/components/ResultsPageElements";
import PaginationPage from "../../src/components/PaginationPage";

export default function ClientsPage() {
    return (
        <>
            <Head>
                <title>CRUD</title>
                <link rel="icon" href="../../public/favicon.ico"/>
            </Head>
            <nav className="navbar navbar-dark bg-dark">
                <Container>
                    <span className="navbar-brand" style={{margin: 0}}>CRUD application</span>
                </Container>
            </nav>
            <Container className="mt-5">
                <PaginationPage>
                    {(i) => (
                        <ResultsPageElements
                            index={i}
                        />
                    )}
                </PaginationPage>
            </Container>
        </>
    )
}