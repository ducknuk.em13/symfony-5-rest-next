import {Container, Stack, Typography} from "@mui/material";
import * as React from "react";
import ClientPageById from "../../src/components/ClientPageById";
import {useRouter} from "next/router";
import useSWR from "swr";
import PaginationPage from "../../src/components/PaginationPage";
import ResultsPageElements from "../../src/components/ResultsPageElements";

export default function ClientIdPage({token, id}) {
    const router = useRouter()
    const { client_id } = router.query
    let url = `http://127.0.0.1:8000/api/clients/${client_id}`;
    const fetcher = (url) =>
        fetch(url, {
            method: 'GET',
            headers: {
                Authorization: `Bearer ${token}`,
                'Content-Type': 'application/json',
            }})
            .then((res) => res.json());
    const { data , error } = useSWR(url, fetcher);

    if (error){
        return (
            <Typography variant='h5'>
                Произошла ошибка при загрузке данных... Попробуйте повторить позднее
            </Typography>
        )
    }
    if (data && data.code === 401 ){
        return (
            <Typography variant='h5'>
                JWT Token not found
            </Typography>
        )
    }

    return (
        <>
            <nav className="navbar navbar-dark bg-dark">
                <Container>
                    <span className="navbar-brand" style={{margin: 0}}>CRUD application</span>
                </Container>
            </nav>

            <Container maxWidth='xl' sx={{ py: 6, px: 1 }}>
                <Stack spacing={3}>
                    {data ?
                        <ClientPageById
                            id = {data.id}
                            name = {data.name}
                            email = {data.email}
                            phone = {data.phone}
                            extraInfo= {data.extraInfo}
                            utm_source = {data.analytics ? data.analytics.utm_source : null}
                            utm_medium = {data.analytics ? data.analytics.utm_medium : null}
                            utm_campaign = {data.analytics ? data.analytics.utm_campaign : null}
                            utm_content = {data.analytics ? data.analytics.utm_content : null}
                            utm_term = {data.analytics ? data.analytics.utm_term : null}
                            token = {token}
                        /> :
                        <Typography variant='h5'>
                            Loading...
                        </Typography>
                    }
                </Stack>
            </Container>
        </>
    )
}
export function getServerSideProps({ req, res })  {
    return {
        props: {
            token: req.cookies.TokenJWT || ""
        }
    }
}