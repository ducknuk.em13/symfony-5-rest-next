import cookie from "cookie";

export default (req, res) => {
    res.setHeader(
        "Set-Cookie",
        cookie.serialize("TokenJWT", req.body.token, {
            httpOnly: true,
            secure: process.env.NODE_ENV !== "development",
            maxAge: 60 * 60,
            sameSite: "strict",
            path: "/",
        })
    );
    res.statusCode = 200;
    res.json({ success: true });
};

// import {sign} from "jsonwebtoken";
// import {serialize} from "cookie";
// import axios from "axios";
//
// const SECRET = process.env.SECRET;
//
// export default async function (req, res) {
//     const {email, password} = req.body;
//     const userLoginData = { email: email, password: password };
//     const result = await fetch("http://127.0.0.1:8000/api/auth/token/login", {
//         body: JSON.stringify(userLoginData),
//         headers: {
//             "Content-Type": "application/json"
//         },
//         method: "POST"
//     })
//     let data = await result.json()
//     if (data.code != 401) {
//         const token = sign(
//             {
//                 exp: Math.floor(Date.now() / 1000) + 60 * 60 * 24 * 30, // 30 days
//                 token: data.token,
//             },
//             SECRET
//         );
//
//         const serialised = serialize("TokenJWT", token, {
//             httpOnly: true,
//             secure: process.env.NODE_ENV !== "development",
//             sameSite: "strict",
//             maxAge: 60 * 60 * 24 * 30,
//             path: "/",
//         });
//
//         res.setHeader("Set-Cookie", serialised);
//
//         res.status(200).json({message: "Success!", body: data});
//     } else {
//         res.json({message: "Invalid credentials!"});
//     }
// }
