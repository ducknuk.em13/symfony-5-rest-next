import Head from "next/head";
import {Container, Stack} from "@mui/material";
import * as React from "react";
import ResultsPageElements from "../src/components/ResultsPageElements";
import PaginationPage from "../src/components/PaginationPage"
import InputClientForm from "../src/components/create/InputClientForm";
import CustomButton from "../src/components/button/CustomButton";
import {useState} from "react";

export default function HomePage({ data }) {
    const [order, setOrder] = useState("");
    return (
        <>
            <Head>
                <title>CRUD</title>
                <link rel="icon" href="../public/favicon.ico"/>
            </Head>
            <nav className="navbar navbar-dark bg-dark">
                <Container>
                    <span className="navbar-brand" style={{margin: 0}}>CRUD application</span>
                </Container>
            </nav>
            <Container maxWidth='xl' sx={{ py: 6, px: 1 }}>
                <Stack spacing={3}>
                    <InputClientForm
                        token={data ? data : null}
                    />
                    <CustomButton variant='contained'
                                  onClick={() => setOrder('order[analytics.utm_source]=desc')}
                                  sx={{marginLeft: 'auto', flexShrink: 0, fontWeight: 600}}
                    >Order by Source </CustomButton>
                    <CustomButton variant='contained'
                                  onClick={() => setOrder('order[analytics.utm_medium]=desc')}
                                  sx={{marginLeft: 'auto', flexShrink: 0, fontWeight: 600}}
                    >Order by Medium </CustomButton>
                    <PaginationPage>
                        {(i) => (
                            <ResultsPageElements
                                index={i}
                                token={data ? data : null}
                                order={order}
                            />
                        )}
                    </PaginationPage>
                </Stack>
            </Container>
        </>
    )
}
export function getServerSideProps({ req, res })  {
    return {
        props: {
            data: req.cookies.TokenJWT || ""
        }
    }
}
