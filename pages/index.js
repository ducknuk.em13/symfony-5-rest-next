import Head from "next/head";
import {Container} from "@mui/material";
import * as React from "react";
import LoginForm from "../src/components/login/LoginForm";

export default function Index() {
    return (
        <>
            <Head>
                <title>CRUD</title>
                <link rel="icon" href="../public/favicon.ico"/>
            </Head>
            <nav className="navbar navbar-dark bg-dark">
                <Container>
                    <span className="navbar-brand" style={{margin: 0}}>CRUD application</span>
                </Container>
            </nav>
            <Container className="mt-5">
                    <LoginForm/>
            </Container>
        </>
    )
}